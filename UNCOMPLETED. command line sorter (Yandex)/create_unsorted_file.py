import random
import string
import click
import datetime


def random_date():
    return datetime.datetime.fromtimestamp(random.randint(1426834229, 1489992629))


def random_email():
    return ''.join([random.choice(string.lowercase) for _ in range(random.randint(1, 10))] +
                   [random.choice(['@gmail.com', '@yandex.ru'])])


class FileSizeParamType(click.ParamType):
    name = 'file size in KB'

    def convert(self, value, param, ctx):
        return int(value) * 1024


@click.command()
@click.option('-f', '--file', 'file_name', help='name of file', default='unsorted_file.csv')
@click.option('-s', '--size', 'size', help='size of generated file', default=500, type=FileSizeParamType())
def main(size, file_name='unsorted_file.csv'):
    with open(file_name, 'w') as f:
        actual_size = 0
        while actual_size < size:
            line = ' '.join([random_email(), random_date().isoformat(), str(random.randint(100, 10000)), '\n'])
            f.write(line)
            actual_size += len(line)


if __name__ == '__main__':
    main()
