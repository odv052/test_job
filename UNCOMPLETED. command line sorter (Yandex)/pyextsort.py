# coding=utf-8
import click
from datetime import datetime
import multiprocessing

TMP_FOLDER = 'tmp_sorted_bucket/'
TMP_FILE_NAME_PATTERN = 'tmp_part_%d.csv'


def sort_chunk_to_file(in_lines, middle_file_name):
    """
    Sort list of lines and write it to file
    :param in_lines: unsorted list of files
    :param middle_file_name: name of file for sorted lines
    """
    memory = 0
    with open(middle_file_name, 'w') as out_file:
        for line in sorted(in_lines, key=lambda line: datetime.strptime(line[1], '%Y-%M-%dT%H:%m:%S')):
            out_file.write(line)
            memory += len(line)


def merge_sorted_files(file_names, out_file):
    """
    Merge sorted files into one big files
    :param file_names: list of sorted file names
    :param out_file: merged file
    """
    files = []
    for file_name in file_names:
        f = open(file_name)
        files.append([f, f.readline()])
    while files:
        m = min(files, key=lambda i: i[1])
        line = m[1]
        m[1] = m[0].readline()
        if not m[1]:
            del files[files.index(m)]
        out_file.write(line)


# @coroutine
def read_lines_per_size(input_file):
    """
    This corounite getting available memory in bytes and returns readed lines
    :param input_file: file for reading
    :return: readed lines
    """
    char = True  # there is True because we wont to start our following loop
    available_memory = 0
    lines = None
    line = ''
    while char:
        available_memory += yield lines
        lines = []
        while available_memory and char:
            # read one line
            while not line.endswith('\n') and available_memory > 0:
                char = input_file.read(1)
                if not char:
                    break
                line += char
                available_memory -= 1

            # add or not last line to unsorted list
            if line.endswith('\n'):
                lines.append(line)
                line = ''


class FileSizeParamType(click.ParamType):
    name = 'file size in KB'

    def convert(self, value, param, ctx):
        return int(value) * 1024


class CpusParamType(click.ParamType):
    name = 'cpus'

    def convert(self, value, param, ctx):
        value = int(value)
        if value < 0:
            raise # TODO raise some specific way. click for that!
        if value > multiprocessing.cpu_count():
            print ("WARNING! Selected number of cpus more than real cpus") # TODO change alerting way
        return value


@click.command()
@click.option('-c', '--cpus', 'cpus', default=4, help='', type=CpusParamType())
@click.option('-l', '--memory_limit', 'memory_limit', help='memory limit in KB', type=FileSizeParamType())
@click.option('-s', '--strict_mode', 'strict_mode', help='will crashed on invalid string', type=click.BOOL())
@click.argument('input_file', type=click.File())
@click.argument('output_file', type=click.File())
# TODO is_valid for cpus. False if cpus < multithreading.cpu_count()
def main(input_file, output_file, memory_limit, cpus, strict_mode):
    used_memory = 0
    sorted_part = 0
    cpus -= 1
    pool = multiprocessing.Pool(cpus)
    chunk = memory_limit / cpus
    parts = memory_limit / chunk + 1 if memory_limit % chunk else 0
    corouted_reader = read_lines_per_size(input_file)
    next(corouted_reader)
    for part in range(parts):
        pool.apply_async(sort_chunk_to_file, corouted_reader.send(chunk), TMP_FILE_NAME_PATTERN % part)
        sort_chunk_to_file(in_lines=corouted_reader.send(chunk), middle_file_name=TMP_FILE_NAME_PATTERN % part)
        used_memory += chunk


if __name__ == '__main__':
    pass
