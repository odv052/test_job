def super_deco(method):
    def wrapped(self, *args, **kwargs):
        all_args_values = list(args) + kwargs.values()
        key = hash(repr(all_args_values))
        if key not in self.cache:
            self.cache[key] = method(self, *args, **kwargs)
        return self.cache[key]

    return wrapped


class MegaClass(object):
    cache = {}

    @super_deco
    def m(self, *args, **kwargs):
        print 'in m'
        return [a for a in args] + [b for k, b in kwargs.items() if 3 < k < 9]


if __name__ == '__main__':
    mc = MegaClass()

    print mc.m(1, 2, 3, [3, 4, 5])
    print mc.m(1, 2, 3, [3, 4, 5])
    print mc.m(1, 2, 3, [3, 4, 5])
    print mc.m(1, 2, 3)
