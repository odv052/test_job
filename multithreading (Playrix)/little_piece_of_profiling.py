import cProfile
from tabulate import tabulate
from runme import parse_zip_folder
import pstats
from os import listdir

if __name__ == '__main__':
    profiles = {}

    profile_folder = 'profiles'

    # profile parse zips with several processes
    for i in range(1, 15):
        cProfile.run('parse_zip_folder("zips", %d)' % i, profile_folder + '/500-1000_%d_processes.profile' % i)

    # print num_proc - time table
    proc_time = {int(name.split('_')[1]): pstats.Stats('/'.join([profile_folder, name])).total_tt
                 for name
                 in filter(lambda x: '.profile' in x, listdir(profile_folder))}

    print tabulate(proc_time.iteritems(), headers=['Processes', 'Time'])

