from zipfile import ZipFile, is_zipfile
from multiprocessing import Pool
from os import listdir
from lxml import etree
import random
import string
import csv


def random_string(n):
    return ''.join(random.choice(string.ascii_letters) for _ in range(n))


def create_xml(random_uniq_string):
    root = etree.Element('root')
    root.append(etree.Element('var', name='id', value=random_uniq_string))
    root.append(etree.Element('var', name='level', value=str(random.randint(0, 100))))
    root.append(etree.Element('objects'))

    for i in range(1, random.randint(1, 10)):
        root[-1].append(etree.Element('object', name=random_string(10)))

    return etree.tostring(root, pretty_print=True)


def create_zip_file(zip_name, uniq_strings):
    with ZipFile(zip_name, 'w') as archive:
        for uniq_string in uniq_strings:
            archive.writestr(uniq_string + '.xml', create_xml(uniq_string))


def create_archives(folder, quantity_zip_files, quantity_xml_files_in_zip):
    """
    create zip files in folder
    :param folder:
    :return:
    """
    uniq_strings = set()
    # create uniq set of strings
    while len(uniq_strings) < quantity_xml_files_in_zip * quantity_zip_files:
        uniq_strings.add(random_string(15))
    uniq_strings = list(uniq_strings)
    # create zip files
    for i in range(quantity_zip_files):
        create_zip_file('%s/archive_%d.zip' % (folder, i),
                        uniq_strings[i * quantity_xml_files_in_zip:(i + 1) * quantity_xml_files_in_zip])


def parse_xml(xml):
    """
    fetching id, level, object names from xml string
    :param xml: string with xml from file
    :return: id, level and list of object names
    """
    root = etree.fromstring(xml)
    return root[0].get('value'), root[1].get('value'), [tag.get('name') for tag in root[2].getchildren()]


def parse_zip(zip_path):
    """
    fetching id-level and id-name lists from zip file
    :param zip_path: path to zip file
    :return: id_level, id_name lists
    """
    with ZipFile(zip_path, 'r') as archive:
        id_level = []
        id_name = []
        for name in archive.namelist():
            xml_id, level, obj_names = parse_xml(archive.read(name))
            id_level.append((xml_id, level))
            id_name.extend(((xml_id, obj_name) for obj_name in obj_names))
    return id_level, id_name


def parse_zip_folder(folder, proc_num):
    """
    parsing all zip files in folder
    :param folder: path to folder
    :return: nothing. Just write csv files in working directory
    """
    archives = ["%s/%s" % (folder, archive) for archive in listdir(folder) if is_zipfile("%s/%s" % (folder, archive))]
    with open('id-level.csv', 'wb') as id_level_csv, open('id-name.csv', 'wb') as id_name_csv:
        id_level_writer = csv.writer(id_level_csv)
        id_name_writer = csv.writer(id_name_csv)
        pool = Pool(processes=proc_num)
        for id_level, id_name in pool.imap_unordered(parse_zip, archives):
            id_level_writer.writerows(id_level)
            id_name_writer.writerows(id_name)


def main(folder, quantity_zip_files, quantity_xml_files_in_zip, proc_num):
    create_archives(folder, quantity_zip_files, quantity_xml_files_in_zip)
    parse_zip_folder(folder, proc_num)


if __name__ == '__main__':
    zip_dir = 'zips'
    quantity_zip_files = 50
    quantity_xml_files_in_zip = 100
    proc_num = 8

    main(zip_dir, quantity_zip_files, quantity_xml_files_in_zip, proc_num)
