import re


# https://docs.python.org/2/howto/functional.html
def coroutine(c):
    def wrapper(*args, **kwargs):
        gen = c(*args, **kwargs)
        next(gen)
        return gen
    return wrapper


@coroutine
def sentence_builder(delimiters):
    delimiter_pattern = re.compile(delimiters)
    end_pattern = re.compile('.*[%s]\s*$' % delimiters)
    line = None
    uncompleted_sentence = ''
    sentences_storage = []
    while True:
        if line:
            if uncompleted_sentence:
                line = ' '.join([uncompleted_sentence, line])
                uncompleted_sentence = ''
            sentences = list(filter(None, map(lambda s: s.strip(), delimiter_pattern.split(line))))
            if not end_pattern.match(line) and sentences:
                uncompleted_sentence = sentences.pop(-1)
            sentences_storage.extend(sentences)
            line = yield
        else:
            line = yield sentences_storage.pop(0) if sentences_storage else None


in_list = ["Hello", "how are you.", "Call me later... Can I trust you? Ok."]

builder = sentence_builder('\.\.\.|\.|\?')

result = []
for part in in_list + ['none'] * 2:
    builder.send(part)
    sentence = next(builder)

    if sentence:
        result.append(sentence)

assert result == ['Hello how are you', 'Call me later', 'Can I trust you', 'Ok']
