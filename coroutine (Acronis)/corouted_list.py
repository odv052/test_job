from coroutine import coroutine

@coroutine
def corouted_l():
    """
    This function is unused in test task. But I just thought that it's cool thing to mention in my repo.
    This is a special coroutine, which can be used for aggregation anything (by send method) and alternately
    returning thing by thing (by next method).
    """
    storage = []
    fresh_list = []
    while True:
        if isinstance(fresh_list, list):
            storage.extend(fresh_list)
            fresh_list = yield
        else:
            fresh_list = yield storage.pop(0) if storage else None

# TODO fix documentation and add test for clarifying
