from multiprocessing import Pool
from time import sleep
from functools import partial


def f(i):
    print i
    sleep(1)
    return i, 12 ** (i*10)


def p(d, i):
    d.append(i)
    print(i)

pl = Pool(4)
l = []
for i in range(4):
    r = pl.apply_async(f, (i,), callback=partial(p, l))
while True:
    if l:
        print 'from main. ended %d: %d' % (l[0][0], l[0][1])
        l.pop(0)
    # sleep(0.05)

print 'END'
