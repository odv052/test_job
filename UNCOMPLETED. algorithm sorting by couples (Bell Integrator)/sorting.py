def swap(state, positions):
    """
    Actual swap of 2 chunks
    """
    new_state = list(state)
    p1, p2 = positions
    new_state[p1:p1+2], new_state[p2:p2+2] = new_state[p2:p2+2], new_state[p1:p1+2]
    return ''.join(new_state)


def search(state, states, positions, queue):
    """
    Try to swap pairs from positions with respect to current state
    """
    new_state = swap(state, positions)
    if new_state not in states:
        states[new_state] = positions
        queue.append(new_state)


def sort_by_couples(a, n=6, k=2):
    """

    :param a: string for sorting
    :param n: number of unique elements
    :param k: size of chunks
    """
    sorted_string = ''.join([str(x) for x in range(1, n+1)])

    states = {sorted_string: ()}  # permutation: (pair1_start, pair2_start)
    queue = [sorted_string]  # used for implementing BFS
    moves = []  # all possible moves with respect to n and k

    # filling possible moves
    for i in range(n - 2*k + 1):
        for j in range(i + k, n-k + 1):
            moves.append((i, j))

    # actual BFS - searching until our queue contains elements
    while queue:
        for move in moves:
            search(queue[0], states, move, queue)
        queue.pop(0)

    if a not in states:
        print("Cannot be sorted")
    else:
        # walking through the solution tree to form an answer
        while states[a]:
            p1, p2 = states[a]
            print('{0} {1}<-->{2}'.format(a, a[p1:p1+2], a[p2:p2+2]))
            a = swap(a, states[a])

if __name__ == '__main__':
    sort_by_couples('124365')
