import click

# n - number of unique elements
# k - size of chunks
# states - permutation : (pair1_start, pair2_start)
# queue - used for implementing BFS
# moves - all possible moves with respect to n and k


def is_correct_couple(a, b):
    a = int(a)
    b = int(b)
    if b-a == 1:
        return 2
    elif a-b == 1:
        return 1
    else:
        return 0


def main(s=None, n=6, k=2, check_possibility=False):
    # 123..n
    sorted_string = ''.join([str(x) for x in range(1, n+1)])

    states = {sorted_string: ()}
    queue = [sorted_string]
    moves = []

    # filling possible moves
    for i in range(n - 2*k + 1):
        for j in range(i + k, n-k + 1):
            moves.append((i, j))


    # actual swap of 2 chunks
    def swap(state, positions):
        new_state = list(state)
        p1, p2 = positions
        new_state[p1:p1+2], new_state[p2:p2+2] = new_state[p2:p2+2], new_state[p1:p1+2]
        return ''.join(new_state)


    # try to swap pairs from positions with respect to current state
    def search(state, positions):
        new_state = swap(state, positions)
        if new_state not in states:
            states[new_state] = positions
            queue.append(new_state)
        else:
            return

    # actual BFS - searching until our queue contains elements
    while queue:
        for move in moves:
            search(queue[0], move)
        queue.pop(0)

    # output
    # a - initial data
    a = s

    if check_possibility:
        if a not in states:
            return False
        else:
            return True

    if a not in states:
        print("Cannot be sorted")
    else:
        # walking through the solution tree to form an answer
        while states[a]:
            p1, p2 = states[a]
            couples = [a[i:i+2] for i in range(0, 5, 2)]
            sa = '{} {} {}'.format(*couples)
            correct_couples = ' '.join(map(lambda c: str(is_correct_couple(*c)), couples))
            print('{1} | {0} | {2}<-->{3}'.format(sa, correct_couples, a[p1:p1+2], a[p2:p2+2]))
            a = swap(a, states[a])


def interactive_mode():
    while True:
        v = raw_input('Your value: ')
        v = v.replace(' ', '').replace('.', '')
        if v.isdigit() and len(v) == 6:
            main(v)
            print
        else:
            print("Wrong value: %s" % v)


def brute_analysis():
    from itertools import permutations
    l = []
    print "Full list:"
    for i in permutations('123456', 6):
        i = ''.join(i)
        if main(i, check_possibility=True):
            l.append(i)
            print i
    print('Len of list: %d' % len(l))


@click.command()
@click.option('-i', type=click.BOOL)
@click.option('-m')
@click.option('-b')
def manage(i, b, m):
    if i:
        interactive_mode()
    elif m:
        main(m)
    elif b:
        brute_analysis()

if __name__ == '__main__':
    manage()
