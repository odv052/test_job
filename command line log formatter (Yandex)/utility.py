#!/usr/bin/python
from jinja2 import Template
import sys
import json
import click

DEFAULT_FORMAT = '{@timestamp} {@fields[level]} {@message}'


def find_attr(obj, path):
    if not path or not isinstance(obj, dict):
        return
    # check key in obj
    if path[0] in obj:
        if path[1:]:
            return find_attr(obj[path[0]], path[1:])
        else:
            return obj[path[0]]


def filter_line(j_line, filters):
    return all(str(find_attr(j_line, path)) == value for path, value in filters)


class FilterTupleParamType(click.ParamType):
    name = 'filter_tuple'

    def convert(self, value, param, ctx):
        path, value = value.split('=')
        return path.split('.'), value


class Jinja2TemplateParamType(click.ParamType):
    name = 'jinja2_template'

    def convert(self, value, param, ctx):
        with open(value) as template_file:
            return Template(template_file.read())


@click.command()
@click.option('-f', '--filter', 'filters', default=None, multiple=True, type=FilterTupleParamType(),
              help='filter lines. This parameter can be used multiple times.')
@click.option('-t', '--format', 'template', default=None, type=Jinja2TemplateParamType()
              , help="format of output. You can define Jinja2 template in which takes all values from 'json'. "
                     "For example: {{ json['@timestamp'] }}")
def main(filters=None, template=None):
    if template:
        output = lambda json_line: template.render(json=json_line)
    else:
        output = lambda json_line: DEFAULT_FORMAT.format(**json_line)

    for line in sys.stdin.xreadlines():
        json_line = json.loads(line)
        if filters and not filter_line(json_line, filters):
            continue
        click.echo(output(json_line))


if __name__ == '__main__':
    main()
